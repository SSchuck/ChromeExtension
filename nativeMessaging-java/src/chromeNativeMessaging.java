import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.KeyStore;
//import java.security.KeyStoreException;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class chromeNativeMessaging {
	private String PATH;
	private String PATH_LOG;
	chromeNativeMessaging(){
		PATH = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		File f=new File(PATH);
		PATH=f.getParent();
		PATH_LOG =PATH+"//hostlog.txt";
	}
	
	@SuppressWarnings("unused")
	private  void log(String message) {
	    File file = new File(PATH_LOG);

	    try {
	      if (!file.exists()) {
	        file.createNewFile();
	      }

	      FileWriter fileWriter = new FileWriter(file.getAbsoluteFile(), true);
	      BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

	      DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	      Date date = new Date();

	      bufferedWriter.write(dateFormat.format(date) + ": " + message + "\r\n");
	      bufferedWriter.close();
	    } catch (Exception e) {
	      log("ERROR ==> Method (log)" + e.getMessage());
	      e.printStackTrace();
	    }
	  }
	public int getInt(byte[] bytes) {
	    return (bytes[3] << 24) & 0xff000000 | (bytes[2] << 16) & 0x00ff0000
	        | (bytes[1] << 8) & 0x0000ff00 | (bytes[0] << 0) & 0x000000ff;
	  }
	private String readMessage(InputStream in) throws IOException {
	    byte[] b = new byte[4];
	    in.read(b);

	    int size = getInt(b);
	    //log(String.format("The size is %d", size));

	    if (size == 0) {
	      throw new InterruptedIOException("Blocked communication");
	    }

	    b = new byte[size];
	    in.read(b);

	    return new String(b, "UTF-8");
	  }
	
	public byte[] getBytes(int length) {
    	byte[] bytes = new byte[4];
    	bytes[0] = (byte) (length & 0xFF);
    	bytes[1] = (byte) ((length >> 8) & 0xFF);
    	bytes[2] = (byte) ((length >> 16) & 0xFF);
    	bytes[3] = (byte) ((length >> 24) & 0xFF);
    	return bytes;
	}
	public void sendMessage(String message)throws IOException{
		  System.out.write(getBytes(message.length()));
		  System.out.write(message.getBytes("UTF-8"));
		  System.out.flush();
		  
	}
	
	public void refreshCerts() throws Exception{
		Path standard = Paths.get(PATH+"//eu_ks");
		File standard_file=standard.toFile();
		Path alt = Paths.get(PATH+"//eu_ks_alt");
		if(standard_file.exists()){
			Files.move(standard,alt,StandardCopyOption.REPLACE_EXISTING);
		}
		//log("datei austausch gestartet");
		TrustedXmlParser txp=new TrustedXmlParser();
		ArrayList<Certificate> given_certs= txp.openDocument();
		KeyStore ks=KeyStore.getInstance("JKS");
		String keyStorePassword ="Taby92";
		OutputStream os;
		ks.load(null, null);
		for(int i=0;i<given_certs.size();i++){
			ks.setCertificateEntry("nummer"+i, given_certs.get(i));
		}
		//log(this.PATH);
		File folder=new File(this.PATH+"//CertificatesNotInXML");
		//log(folder.getAbsolutePath());
		if (folder.exists()){
			for(File f:folder.listFiles()){
				if(f.getPath().endsWith(".cer")){
					CertificateFactory cf=CertificateFactory.getInstance("X.509");
					byte [] decoded = Base64.getDecoder().decode(Files.readAllBytes(Paths.get((f.getAbsolutePath()))));
					InputStream is=new ByteArrayInputStream(decoded); 
					Certificate cs=cf.generateCertificate(is);
					ks.setCertificateEntry(f.getName(),cs);
				}
			}
		}
		//log("try to save");
		standard_file.createNewFile();
		os = new FileOutputStream(PATH+"//eu_ks",false);
		ks.store(os,keyStorePassword.toCharArray());
		Files.deleteIfExists(alt);
	}
	@SuppressWarnings("unused")
	public void getCertFrom(String aURL) throws Exception {
		File alt=new File(PATH+"//eu_ks_alt");
		File neu=new File(PATH+"//eu_ks");
		Date date_alt=new Date(alt.lastModified());
		Date date_neu=new Date(neu.lastModified());
		File file_in_use;
		if(alt.exists()&&date_alt.after(date_neu)){
			file_in_use=alt;
		}else{
			file_in_use=neu;
		}
		KeyStore ks=KeyStore.getInstance("JKS");
		String keyStorePassword ="Taby92";
		InputStream keyStream= new FileInputStream(file_in_use);
		ks.load(keyStream,keyStorePassword.toCharArray());
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("PKIX");
		kmf.init(ks, keyStorePassword.toCharArray());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(ks);
		SSLContext ctx = SSLContext.getInstance("TLS");
		ctx.init(kmf.getKeyManagers(),tmf.getTrustManagers(),new java.security.SecureRandom());
		SSLSocketFactory sf =ctx.getSocketFactory();
		Security.setProperty("ocsp.enable","true");
		System.setProperty("com.sun.security.enable.CRLDP", "true");
		HttpsURLConnection.setDefaultSSLSocketFactory(sf);
		URL destinationURL = new URL(aURL);	
       	HttpsURLConnection conn = (HttpsURLConnection) destinationURL.openConnection();
       	conn.connect();
       	Certificate[] certs = conn.getServerCertificates();
       	conn.disconnect();
       	//log("nb = " + certs.length);
       	
       	for (Certificate cert : certs) {
           	//log("Certificate is: " + cert);
           	/**if(cert instanceof X509Certificate) {
               	try {
                   	( (X509Certificate) cert).checkValidity();
                   	System.out.println("Certificate is active for current date");
                   	FileOutputStream os = new FileOutputStream("/home/sebastien/Bureau/myCert"+i);
                   	i++;
                   	os.write(cert.getEncoded());
               	} catch(CertificateExpiredException cee) {
                   	System.out.println("Certificate is expired");
               	}
           	} else {
               	System.err.println("Unknown certificate type: " + cert);
           	}*/
       	}
	}
	@SuppressWarnings("deprecation")
	static public void main(String[] args)
	{
    final chromeNativeMessaging app = new chromeNativeMessaging();
    String _url = null;
    try {
		_url = app.readMessage(System.in);
	} catch (IOException e) {
		//app.log(e.getMessage());
	}
	_url=_url.substring(9,_url.length()-2);
	//app.log(_url);
	String _o =null;
	File file=new File(app.PATH+"\\eu_ks");
	File file_alt=new File(app.PATH+"//eu_ks_alt");
	file.getParentFile().mkdirs();
		if(!file.exists()){
			//app.log("euks extistiert nicht");
			if(!file_alt.exists()){
				try {
					app.refreshCerts();
				}catch (Exception e0){
					//app.log("Fehler in refresh Cert");
					//app.log(e0.getMessage());
				}
			}
		}else{
			Date dt=new Date(file.lastModified());
			Date yesterday= new Date();
			yesterday.setDate(yesterday.getDate()-1);
			if(yesterday.after(dt)){
				try{
					app.refreshCerts();
				}catch (Exception e){
					try{
						Files.deleteIfExists(file_alt.toPath());
					}catch (Exception e9) {
					//	app.log("Fehler beim delete");
					}
				}
			}
		}
	try {
		app.getCertFrom(_url);
		_o="true";
		} catch (Exception e1) {
			//app.log(e1.getMessage());
			_o="false";
		}
	try {
		app.sendMessage(_o);
	} catch (IOException e) {
		//app.log(e.getMessage());
	}
}
}